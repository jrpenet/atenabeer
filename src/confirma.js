import {getPedidos, criaPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagatemp } from './temp'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
})();

cartTable()
insereCart()
rodapeCart()
apagatemp()

//console.log(getPedidos().filter((x) => x.produto === 'Embalagem').length)

if(getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 && getPedidos().filter((x) => x.produto.includes('Batata Beer')).length > 0 || getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 && getPedidos().filter((x) => x.produto.includes('Batata Tradicional')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Anéis de Cebola')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Bolinhos de Charque')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Bolinhos de Queijo')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Calabresa com Fritas')).length > 0 || getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Camarão Alho e Óleo')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Camarão Empanado')).length > 0 || getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Carne de Sol com Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Carne de Sol, Queijo e Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Coração com Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Coxinha Crocante')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Asinha Crocante')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Macaxeira Frita')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Macaxeira Frita com Carne de Sol')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Maminha com Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Maminha com Queijo e Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Pão de Alho')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Carne c/ Cheddar')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Queijo')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Queijo e Presunto')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Charque com Cream Cheese')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Picanha com Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Picanha com Queijo e Fritas')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Posta de Albacora')).length > 0 ||getPedidos().filter((x) => x.produto === 'Embalagem').length < 1 &&  getPedidos().filter((x) => x.produto.includes('Tripinha')).length > 0){
    criaPedidos(1, 'Embalagem', 1.5)
    //console.log('add batata')
}

//console.log(getPedidos().filter((x) => x.produto.includes('Batata Tradicional')))
document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()

    const semTx = getPedidos().filter((x) => x.taxa === 'taxa').length

    //console.log(semTx)

    if(semTx < 1){
        location.assign('./txentrega.html')
        
    }else{

        const valor = getPedidos().filter((x) => x.taxa !== 'taxa').map((e) => e.subt).reduce((a, b) => {
            return a + b
        }, 0)
    
        const retiradaLa = getPedidos().filter((x) => x.taxa === 'taxa')[0].produto === 'Retirar na Loja'

        if(getPedidos().map((x)=> x.taxa).includes('taxa') && valor >= 10 || getPedidos().map((x)=> x.taxa).includes('taxa') && retiradaLa){
            location.assign('./forms.html')
            //console.log('forms')
        }if(valor < 10 && !retiradaLa){
            alert('Fazemos entrega para valores acima de R$ 10,00 sem a taxa de entrega. Continue comprando, clique em Comprar Mais')
        }
    }
})