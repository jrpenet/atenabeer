import {gettemp} from './temp'
import {criaPedidos, getPedidos} from './pedidos'
// import { addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios } from './molhosrecheios'

if(gettemp().length < 1){
    location.assign('./cardapio.html')
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto

//mostra a imagem do pedido
const showOrder = document.querySelector('#imagemApr')
showOrder.setAttribute('src', gettemp()[0].foto)

//pega o q foi digitado em obs
const pegaObs = document.querySelector('#insereObs')

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

//configuração para aparecer as opções de pastel, de acordo com o escolhido
if(gettemp()[0].produto === 'Pastel Tradicional'){
    const pastelTrad = document.querySelector('#pastelTradicional')
    pastelTrad.removeAttribute('style', 'display: none;')
    //alert('ok')
}

if(gettemp()[0].produto === 'Pastel Premium'){
    const pastelTrad = document.querySelector('#pastelPremium')
    pastelTrad.removeAttribute('style', 'display: none;')
    //alert('ok')
}

if(gettemp()[0].produto === 'Pastel Duplo Premium'){
    const pastelTrad = document.querySelector('#pastelDuploPremium')
    pastelTrad.removeAttribute('style', 'display: none;')
    //alert('ok')
}

if(gettemp()[0].produto === 'Pastel Select'){
    const pastelTrad = document.querySelector('#pastelSelect')
    pastelTrad.removeAttribute('style', 'display: none;')
    //alert('ok')
}

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()
    
    if(gettemp()[0].produto === 'Pastel Tradicional'){
        let sabor1 = document.querySelector('#pastelTs1').value
        let sabor2 = document.querySelector('#pastelTs2').value
        let sabor3 = document.querySelector('#pastelTs3').value
        let sabor4 = document.querySelector('#pastelTs4').value
        let sabor5 = document.querySelector('#pastelTs5').value
        let listaSabores = ' Sabores: ' + sabor1 + ', ' + sabor2 + ', ' + sabor3 + ', ' + sabor4 + ', ' + sabor5

        let boxes = document.querySelectorAll('.checado')
        let rs = ''
    
        for(let i = 0; i < 12; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value + ", "
            }
        }

        if(listaSabores == ' Sabores: -, -, -, -, -'){
            alert('Escolha pelo menos 1 sabor')
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + listaSabores.replace(/, -/g, '') + '. - Acomp.: ' + rs + ' - Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            
            location.assign('./confirma.html')
        }

    }else if(gettemp()[0].produto === 'Pastel Premium'){
        let saborPremium = document.querySelector('#pastelPsp').value
        let sabor1 = document.querySelector('#pastelPs1').value
        let sabor2 = document.querySelector('#pastelPs2').value
        let sabor3 = document.querySelector('#pastelPs3').value
        let sabor4 = document.querySelector('#pastelPs4').value
        let sabor5 = document.querySelector('#pastelPs5').value
        let listaSabores = ' Sabores: ' + sabor1 + ', ' + sabor2 + ', ' + sabor3 + ', ' + sabor4 + ', ' + sabor5

        let boxes = document.querySelectorAll('.checado2')
        let rs = ''
    
        for(let i = 0; i < 12; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value + ", "
            }
        }

        if(saborPremium == '-' ){
            alert('Escolha o sabor premium')
        }else if(listaSabores == ' Sabores: -, -, -, -, -'){
            alert('Escolha pelo menos 1 sabor tradicional')
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Premium: '+ saborPremium + listaSabores.replace(/, -/g, '') + '. Acomp.: ' + rs + ' - Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            
            location.assign('./confirma.html')
        }

    }else if(gettemp()[0].produto === 'Pastel Duplo Premium'){
        let saborPremium = document.querySelector('#pastelDPsp1').value
        let saborPremium2 = document.querySelector('#pastelDPsp2').value
        let sabor1 = document.querySelector('#pastelDPs1').value
        let sabor2 = document.querySelector('#pastelDPs2').value
        let sabor3 = document.querySelector('#pastelDPs3').value
        let sabor4 = document.querySelector('#pastelDPs4').value
        let sabor5 = document.querySelector('#pastelDPs5').value
        let listaSabores = '. Sabores: ' + sabor1 + ', ' + sabor2 + ', ' + sabor3 + ', ' + sabor4 + ', ' + sabor5

        let boxes = document.querySelectorAll('.checado3')
        let rs = ''
    
        for(let i = 0; i < 12; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value + ", "
            }
        }

        if(saborPremium == '-' || saborPremium2 == '-'){
            alert('Escolha o sabor premium')
        }else if(listaSabores == '. Sabores: -, -, -, -, -'){
            alert('Escolha pelo menos 1 sabor tradicional')
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabores Premium: '+ saborPremium + ', ' + saborPremium2 + listaSabores.replace(/, -/g, '') + '. Acomp.: ' + rs + ' - Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            
            location.assign('./confirma.html')
        }
        
    } else if(gettemp()[0].produto === 'Pastel Select'){
        let saborSelect = document.querySelector('#pastelS').value
        if(saborSelect === '-'){
            alert('Escolha um sabor')
        }else{

            let boxes = document.querySelectorAll('.checado4')
            let rs = ''
    
            for(let i = 0; i < 12; i++){
                if(boxes[i].checked === true){
                    rs += boxes[i].value + ", "
                }
            }

            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor Select: '+ saborSelect + '. Acomp.: '+ rs + ' - Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            
            location.assign('./confirma.html')
        }
    }else{
        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        location.assign('./confirma.html')
    } 

    

    
    
})