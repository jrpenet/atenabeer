import { criatemp } from './temp'

//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Pastel Tradicional',
    descricao: 'Escolha 5 sabores: Cheddar, catupiry, mussarela, coalho, frango, carne moída, calabresa, presunto e charque.',
    preco: 6,
    foto: './images/pastel.jpeg',
    tipo: 'pastel'
}, {
    nomeHamburguer: 'Pastel Premium',
    descricao: '5 sabores + filé de camarão ou bacon ou coração de frango ou cream cheese ou carne de sol.',
    preco: 7.5,
    foto: './images/pastel.jpeg',
    tipo: 'pastel'
}, {
    nomeHamburguer: 'Pastel Duplo Premium',
    descricao: '5 sabores tradicionais + 2 sabores premium: Camarão, bacon, coração de frango, cream cheese ou carne de sol.',
    preco: 8.5,
    foto: './images/pastel.jpeg',
    tipo: 'pastel'
}, {
    nomeHamburguer: 'Pastel Select',
    descricao: 'Escolha entre: Só filé de camarão, só carne de sol, só bacon, só coração de frango ou só cream cheese.',
    preco: 9.5,
    foto: './images/pastel.jpeg',
    tipo: 'pastel'
}, {
    nomeHamburguer: 'Caldinho - Camarão',
    descricao: '',
    preco: 6.5,
    foto: './images/caldinhocamarao.png',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Caldinho - Macaxeira',
    descricao: '',
    preco: 6.5,
    foto: './images/caldinhomacaxeira.png',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Caldinho - Feijão',
    descricao: '',
    preco: 6.5,
    foto: './images/caldinhofeijao.png',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Caldinho - Sururu',
    descricao: '',
    preco: 6.5,
    foto: './images/caldinhosururu.png',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Caldinho - Peixe',
    descricao: '',
    preco: 6.5,
    foto: './images/caldinhopeixe.png',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Opção 1',
    descricao: 'Camarão alho e óleo + coração de frango + batata frita + mini pasteis de queijo',
    preco: 35.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Opção 2',
    descricao: 'Macaxeira frita + batata frita + maminha + aneis de cebola',
    preco: 39.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Opção 3',
    descricao: 'Picanha + camarão empanado + tripinha + batata frita',
    preco: 43.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Opção 4',
    descricao: 'Pão de alho + bolinho de charque + carne de sol + batata frita',
    preco: 46.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Opção 5',
    descricao: 'Picanha + carne de sol + bolinhos de queijo + batata beer c/ cream cheese, bacon e cheddar',
    preco: 53.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Batata Tradicional',
    descricao: 'Batata frita',
    preco: 9,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Batata Beer',
    descricao: 'Cream cheese, bacon e cheddar',
    preco: 11.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Barca 2',
    descricao: 'Batata tradicional, calabresa acebolada, coração de frango e macaxeira frita',
    preco: 36.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Anéis de Cebola',
    descricao: '',
    preco: 13.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Bolinhos de Charque',
    descricao: 'Porção com 10 unidades',
    preco: 16.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Bolinhos de Queijo',
    descricao: 'Porção com 10 unidades',
    preco: 13.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Calabresa com Fritas',
    descricao: '',
    preco: 17.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Camarão Alho e Óleo',
    descricao: '',
    preco: 21.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Camarão Empanado',
    descricao: '',
    preco: 21.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Carne de Sol com Fritas',
    descricao: '',
    preco: 24.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Carne de Sol, Queijo e Fritas',
    descricao: '',
    preco: 27.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Coração com Fritas',
    descricao: '',
    preco: 21.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Coxinha Crocante',
    descricao: 'Porção com 8 unidades',
    preco: 24.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Asinha Crocante',
    descricao: 'Porção com 8 unidades',
    preco: 19.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Macaxeira Frita',
    descricao: '',
    preco: 9.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Macaxeira Frita com Carne de Sol',
    descricao: '',
    preco: 24.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Maminha com Fritas',
    descricao: '',
    preco: 27.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Maminha com Queijo e Fritas',
    descricao: '',
    preco: 30.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Pão de Alho',
    descricao: '',
    preco: 6.9,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Mini Pastéis - Carne c/ Cheddar',
    descricao: 'Porção com 10 unidades',
    preco: 12.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Mini Pastéis - Queijo',
    descricao: 'Porção com 10 unidades',
    preco: 11.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Mini Pastéis - Queijo e Presunto',
    descricao: 'Porção com 10 unidades',
    preco: 11.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Mini Pastéis - Charque com Cream Cheese',
    descricao: 'Porção com 10 unidades',
    preco: 15.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Picanha com Fritas',
    descricao: '',
    preco: 33.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Picanha com Queijo e Fritas',
    descricao: '',
    preco: 36.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Posta de Albacora',
    descricao: '',
    preco: 19.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Tripinha',
    descricao: '',
    preco: 17.99,
    foto: './images/logo.png',
    tipo: 'petisco'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const opt4 = document.createElement('option')
    const opt5 = document.createElement('option')
    const opt6 = document.createElement('option')
    const opt7 = document.createElement('option')
    const opt8 = document.createElement('option')
    const opt9 = document.createElement('option')
    const opt10 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    opt4.setAttribute('value', '4')
    opt4.textContent = '4'
    opt5.setAttribute('value', '5')
    opt5.textContent = '5'
    opt6.setAttribute('value', '6')
    opt6.textContent = '6'
    opt7.setAttribute('value', '7')
    opt7.textContent = '7'
    opt8.setAttribute('value', '8')
    opt8.textContent = '8'
    opt9.setAttribute('value', '9')
    opt9.textContent = '9'
    opt10.setAttribute('value', '10')
    opt10.textContent = '10'
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    select.appendChild(opt4)
    select.appendChild(opt5)
    select.appendChild(opt6)
    select.appendChild(opt7)
    select.appendChild(opt8)
    select.appendChild(opt9)
    select.appendChild(opt10)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')        
    })

    imagem.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto
        criatemp(qt, prodt, price, tp, '', img)
        location.assign('./conftemp.html')
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto
        criatemp(qt, prodt, price, tp, '', img)
        location.assign('./conftemp.html')
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'pastel').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
        
    })
}

const addCardapioTiaGil = () => {
    cardapio.filter((x) => x.tipo === 'caldinho').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable2').appendChild(hrEl)
        document.querySelector('#docTable2').appendChild(addElements(lanche))
        
    })
}

const addCardapioDoce = () => {
    cardapio.filter((x) => x.tipo === 'petisco').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable3').appendChild(hrEl)
        document.querySelector('#docTable3').appendChild(addElements(lanche))
        
    })
}

const addCardapioSalgada = () => {
    cardapio.filter((x) => x.tipo === 'salgada').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable4').appendChild(hrEl)
        document.querySelector('#docTable4').appendChild(addElements(lanche))
        
    })
}


export {addCardapio, addCardapioTiaGil, addCardapioDoce, addCardapioSalgada, addElements}