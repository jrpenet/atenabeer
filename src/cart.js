import {removePedidos, getPedidos, removePedidos2} from './pedidos'

let pedidos = []

const loadPedidos = function(){
    const pedidosJSON = sessionStorage.getItem('pedidos')
    
    if(pedidosJSON !== null){
        return JSON.parse(pedidosJSON)
    } else {
        return []
    }
}

const cartTable = () => {
    const tabela3 = document.createElement('table')
    document.querySelector('#confirmacao').appendChild(tabela3)

    //header da tabela

    const quantid = document.createElement('th')
    quantid.textContent = 'QTD'
    document.querySelector('table').appendChild(quantid)

    const refere = document.createElement('th')
    refere.textContent = 'ITEM'
    document.querySelector('table').appendChild(refere)

    const unit = document.createElement('th')
    unit.textContent = 'PREÇO'
    document.querySelector('table').appendChild(unit)

    const subt = document.createElement('th')
    subt.textContent = 'SUB-T'
    document.querySelector('table').appendChild(subt)

    const rmv = document.createElement('th')
    rmv.textContent = 'REMOVER'
    document.querySelector('table').appendChild(rmv)
}

const incluiNaDom = (item) => {
    const docElement = document.createElement('tr')
    const tdElement6 = document.createElement('td')
    const tdElement7 = document.createElement('td')
    const tdElement8 = document.createElement('td')
    const tdElement9 = document.createElement('td')
    const tdElement10 = document.createElement('td')
    const button = document.createElement('button')    
    const subTotal = item.qtd * item.preco
        
    tdElement6.textContent = `${item.qtd}`
    docElement.appendChild(tdElement6)

    tdElement7.setAttribute('class', 'item')
    tdElement7.textContent = `${item.produto}`
    docElement.appendChild(tdElement7)

    tdElement8.textContent = `R$${item.preco.toFixed(2).replace('.', ',')}`
    docElement.appendChild(tdElement8)

    tdElement9.setAttribute('class', 'subtotal')
    tdElement9.textContent = `${subTotal}`
    docElement.appendChild(tdElement9)

    docElement.appendChild(tdElement10)
    button.textContent = 'X'

    if(item.produto !== 'Embalagem'){
        tdElement10.appendChild(button)
    }

    button.addEventListener('click', () => {

        const a = pedidos.indexOf(item)
        removePedidos(a)      
        window.location.reload()

                
    })

    if(getPedidos().filter((x)=> x.produto === 'Embalagem') && getPedidos().filter((x) => x.produto.includes('Batata Beer')).length < 1  && getPedidos().filter((x) => x.produto.includes('Batata Tradicional')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Anéis de Cebola')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Bolinhos de Charque')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Bolinhos de Queijo')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Calabresa com Fritas')).length < 1  &&  getPedidos().filter((x) => x.produto.includes('Camarão Alho e Óleo')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Camarão Empanado')).length < 1  &&  getPedidos().filter((x) => x.produto.includes('Carne de Sol com Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Carne de Sol, Queijo e Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Coração com Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Coxinha Crocante')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Asinha Crocante')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Macaxeira Frita')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Macaxeira Frita com Carne de Sol')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Maminha com Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Maminha com Queijo e Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Pão de Alho')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Carne c/ Cheddar')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Queijo')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Queijo e Presunto')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Mini Pastéis - Charque com Cream Cheese')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Picanha com Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Picanha com Queijo e Fritas')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Posta de Albacora')).length < 1 &&  getPedidos().filter((x) => x.produto.includes('Tripinha')).length < 1){
        if(item.produto === 'Embalagem'){
            console.log(`apaga ${item.produto}`)
            const a = pedidos.indexOf(item)
            removePedidos(a)      
            window.location.reload()
        }
       
        
    }

    return docElement
}

const insereCart = () => {
    pedidos.forEach((item) => {
        document.querySelector('table').appendChild(incluiNaDom(item))   
    }) 
}

const rodapeCart = () => {
    const trnew = document.createElement('tr')
    document.querySelector('table').appendChild(trnew)
    

    const tdElTotal = document.createElement('td')
    tdElTotal.setAttribute('colspan', '4')
    tdElTotal.setAttribute('id', 'totalTXT')
    tdElTotal.textContent = "TOTAL"
    trnew.appendChild(tdElTotal)

    const els = document.querySelectorAll('.subtotal')
    let somando = 0;
    [].forEach.call(els, function(el){
        somando += parseInt(el.textContent);
    })

    const valorTotal = document.createElement('td')
    valorTotal.setAttribute('id', 'total')
    trnew.appendChild(valorTotal)
    document.querySelector('#total').textContent = 'R$' + getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')
}

pedidos = loadPedidos()

export {cartTable, insereCart, rodapeCart}